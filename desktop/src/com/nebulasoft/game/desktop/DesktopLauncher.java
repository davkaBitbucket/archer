package com.nebulasoft.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.nebulasoft.game.ArcherGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.title = "Archer: Revenge of the Fallen";
		config.width = 480;
		config.height = 800;
		new LwjglApplication(new ArcherGame(), config);
	}
}
