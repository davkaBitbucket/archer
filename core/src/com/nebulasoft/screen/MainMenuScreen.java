package com.nebulasoft.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.nebulasoft.game.ArcherGame;
import com.nebulasoft.game.Asset;

public class MainMenuScreen implements Screen {
	private ArcherGame game;
	private OrthographicCamera camera;
	private final Rectangle playBounds = new Rectangle(120, 300, 400, 45);
	private final Vector3 touchPoint = new Vector3();
	
	public MainMenuScreen(final ArcherGame game) {
		this.game = game;
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 480, 800);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(250, 250, 250, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		camera.update();
		game.batch.setProjectionMatrix(camera.combined);

		game.batch.begin();
		game.batch.draw(Asset.MENU_IMAGE, 100, 50, 0, 300, 380, 300);
		game.batch.end();
		handleTouch();
	}
	
	private void handleTouch() {
		if (Gdx.input.justTouched()) {
			camera.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
			if (playBounds.contains(touchPoint.x, touchPoint.y)) {
				Asset.playClickSound();
				game.setScreen(new GameScreen(game));
				return;
			}
		}
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		game.dispose();
//		Asset.destroy();
	}
}
